// match_images.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"

#include <direct.h>
#include "match_images.h"


int main(int argc, const char** argv )
{	
	Mat img1 = imread(argv[1],1); 

	Mat img2 = imread(argv[2],1); 

	// if either or both of template image and validation image are not read properly
	if(img1.empty() || img2.empty())
	{
		cout << "check input image" << "\n";

		int temp = 0;

		cin >> temp;  // press any keyboard key and press enter to exit

		return temp;
	}	
	
	string img1_str = argv[1];
	string img2_str = argv[2];

	string img1_path = img1_str.substr(0, img1_str.find_first_of("."));

	size_t slash_pos = img2_str.find_last_of("\\")+1;
	string img2_name = img2_str.substr(slash_pos, img2_str.find_first_of(".")-slash_pos);
	
	cout << "path of image1: " << img1_path << endl;

	cout << "name of image2: " << img2_name << endl;

	double threshold1 = 0.6;
	int threshold2 = 3;
	
	stringstream ss_orb;

	ss_orb << img1_path << "_" << img2_name << ".jpg"; 
	
	orbMatch(img1, img2, ss_orb.str(), threshold1, threshold2);  // output matching image .. if a good match is found
	
	//surfMatch(img1, img2, "siftmatch_tom2_img12.jpg", threshold1, threshold2);
	
	//kazeMatch(im1, im2, "kazematchb12.jpg", threshold1, threshold2);

	return 0;
}