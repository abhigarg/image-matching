# ifndef _FINDLOGO_H_
# define _FINDLOGO_H_

// System Includes
# include <stdio.h>
# include <stdlib.h>
# include <tchar.h>
# include <iostream>
# include <fstream>
# include <io.h>
# include <string>

// OpenCV Includes
# include <opencv2\imgproc\imgproc.hpp>
# include <opencv2\highgui\highgui.hpp>
# include <opencv2\core\core.hpp>
# include <opencv2\contrib\contrib.hpp>
# include <opencv2\calib3d\calib3d.hpp>
# include "opencv2/features2d/features2d.hpp"
# include <opencv2/nonfree/features2d.hpp>
# include <opencv\cv.h>
# include <opencv\highgui.h>
# include <opencv\cvaux.h>
# include <opencv\cxcore.h>

#include <time.h>

// KAZE
#include <KAZE.h>

using namespace cv;
using namespace std;

/** function to find prominent color in the main image **/
vector<Scalar> findProminentColor(Mat &img)
{
	int color_pxl_count = 0;
	int prominent_color_pxl = 0;
	int t = 0;

	Scalar t_min, t_max;

	vector<Scalar> t_minmax;

	// check for blue color
	Scalar t_min_blue = Scalar(0, 140, 140);
	Scalar t_max_blue = Scalar(5, 255, 255);

	Mat out_img;

	inRange(img,t_min_blue,t_max_blue,out_img);

	color_pxl_count = countNonZero(out_img);
	cout << "No of blue pixels found:" << color_pxl_count << "\n";
	
	if(color_pxl_count > 0)
	{
		t_min = t_min_blue;
		t_max = t_max_blue;
	
		prominent_color_pxl = color_pxl_count;
	}


	// check for yellow color
	Scalar t_min_yellow = Scalar(90, 140, 140);
	Scalar t_max_yellow = Scalar(93, 255, 255);

	inRange(img,t_min_yellow,t_max_yellow,out_img);

	color_pxl_count = countNonZero(out_img);

	cout << "No of yellow pixels found:" << color_pxl_count << "\n";
	
	if(prominent_color_pxl < color_pxl_count)
	{
		cout << "prominent color is yellow" << "\n";

		prominent_color_pxl = color_pxl_count;

		t_min = t_min_yellow;
		t_max = t_max_yellow;
	}

	// check for orange color
	Scalar t_min_orange = Scalar(100, 50, 170, 0);
	Scalar t_max_orange = Scalar(105, 180, 256, 0);

	inRange(img,t_min_orange,t_max_orange,out_img);

	color_pxl_count = countNonZero(out_img);

	cout << "No of orange pixels found:" << color_pxl_count << "\n";
	
	if(prominent_color_pxl < color_pxl_count)
	{
		cout << "prominent color is orange" << "\n";

		prominent_color_pxl = color_pxl_count;

		t_min = t_min_orange;
		t_max = t_max_orange;
	}

	// check for red color
	Scalar t_min_red = Scalar(120, 140, 140, 0);
	Scalar t_max_red = Scalar(125, 255, 255, 0);

	inRange(img,t_min_red,t_max_red,out_img);

	color_pxl_count = countNonZero(out_img);

	cout << "No of red pixels found:" << color_pxl_count << "\n";
	
	if(prominent_color_pxl < color_pxl_count)
	{
		cout << "prominent color is red" << "\n";

		prominent_color_pxl = color_pxl_count;

		t_min = t_min_red;
		t_max = t_max_red;
	}


	// check for pink color
	Scalar t_min_pink = Scalar(125, 0, 140, 0);
	Scalar t_max_pink = Scalar(130, 150, 255, 0);

	inRange(img,t_min_pink,t_max_pink,out_img);

	color_pxl_count = countNonZero(out_img);

	cout << "No of pink pixels found:" << color_pxl_count << "\n";
	
	if(prominent_color_pxl < color_pxl_count)
	{
		cout << "prominent color is pink" << "\n";

		prominent_color_pxl = color_pxl_count;

		t_min = t_min_pink;
		t_max = t_max_pink;
	}


	t_minmax.push_back(t_min);
	t_minmax.push_back(t_max);

	cin >> t;
	cout << "\n";

	return t_minmax;

}

void CheckNNDR(const vector<vector<DMatch> > &match_12, vector<vector<DMatch> > &match_21, vector<DMatch> &match_sym, double thresh )
{
    double ratio12, ratio21;
    ratio12 = 0;
    ratio21 = 0;
	
	// Select the most matching features. A lower threshold may miss some features
    //thresh = 0.7;  // threshold for NNDR
    
    for( size_t fwd_ind = 0; fwd_ind < match_12.size(); fwd_ind++ )
    {
        if(match_12[fwd_ind][0].distance < match_12[fwd_ind][1].distance)
            ratio12 = match_12[fwd_ind][0].distance / match_12[fwd_ind][1].distance;

        if(match_12[fwd_ind][0].distance > match_12[fwd_ind][1].distance)
            ratio12 = match_12[fwd_ind][1].distance / match_12[fwd_ind][0].distance;
        
		if(ratio12 <= thresh)
        {
            for( size_t fwd_match_ind = 0; fwd_match_ind < match_12[fwd_ind].size(); fwd_match_ind++ )
            {
                DMatch forward = match_12[fwd_ind][fwd_match_ind];
                
				if(match_21[forward.trainIdx][0].distance < match_21[forward.trainIdx][1].distance)
                    ratio21 = match_21[forward.trainIdx][0].distance/match_21[forward.trainIdx][1].distance;
                
				if(match_21[forward.trainIdx][0].distance > match_21[forward.trainIdx][1].distance)
                    ratio21 = match_21[forward.trainIdx][1].distance/match_21[forward.trainIdx][0].distance;
                
				if(ratio21 <= thresh)
                {
                    for( size_t bkw_ind = 0; bkw_ind < match_21[forward.trainIdx].size(); bkw_ind++ )
                    {
                        DMatch backward = match_21[forward.trainIdx][bkw_ind];
                        
						// look for forward as well as backward matches
						if( backward.trainIdx == forward.queryIdx )
                        {
                            match_sym.push_back(forward);
                        }
                    }
                }
            }
        }
    }
}


/** function to find if logo features matches with shelf image features **/
int surfMatch( Mat &img1, Mat &img2, string output_str, double thresh1, int thresh2)
{
	Mat img_1, img_2; 
	cvtColor(img1, img_1, CV_RGB2GRAY);
	cvtColor(img2, img_2, CV_RGB2GRAY);
	

	if( !img_1.data || !img_2.data || !img1.data || !img2.data )
	{ std::cout<< " --(!) Error reading images " << std::endl; return -1; }

	clock_t tStart = clock();

	//-- Step 1: Detect the keypoints using SURF Detector

	// Parameter: limits features the program will detect (logo and shelf). Too many features could lead wrong matches
	SiftFeatureDetector detector;
	
	//DenseFeatureDetector detector;
	//FeatureDetector detector = new FeatureDetector("ORB");

	vector<KeyPoint> keypoints_1, keypoints_2;

	detector.detect( img_1, keypoints_1 );
	detector.detect( img_2, keypoints_2 );

	cout << "no of SURF keypoints found in ref image: " << keypoints_1.size() << "\n";
	cout << "no of SURF keypoints found in test image: " << keypoints_2.size() << "\n";

	//-- Step 2: Calculate descriptors (feature vectors)
	SiftDescriptorExtractor extractor;

	Mat descriptors_1, descriptors_2;

	//cout << "check surf 0" << endl;

	extractor.compute( img_1, keypoints_1, descriptors_1 );
	
	//cout << "check surf 0.1" << endl;
	
	extractor.compute( img_2, keypoints_2, descriptors_2 );

	//cout << "check surf 0.2" << endl;

	//-- Step 3: Matching descriptor vectors using FlannBasedMatcher matcher
	//setting flann parameters
	const Ptr<flann::IndexParams>& indexParams=new flann::KDTreeIndexParams(16);  //16   5
	const Ptr<flann::SearchParams>& searchParams=new flann::SearchParams(8);     //8   50
	FlannBasedMatcher matcher; //(indexParams, searchParams);
  
	//BFMatcher bfmatch(NORM_HAMMING);

	//cout << "check surf 0.3 " << endl;

	//const Ptr<flann::AutotunedIndexParams>& autoTuneIndexParams = new flann::AutotunedIndexParams(0.9, 0.01, 0, 0.4);

	//FlannBasedMatcher matcher(autoTuneIndexParams);

	vector< DMatch > good_matches;
	vector<vector<DMatch> > match_12, match_21;

	matcher.knnMatch(descriptors_1, descriptors_2, match_12, 2);

	//cout << "check surf 0.4 " << endl;

	matcher.knnMatch(descriptors_2, descriptors_1, match_21, 2);

	//cout << "check surf1" << endl;

	if(match_12.size()>0 && match_21.size()>0)
		CheckNNDR(match_12, match_21, good_matches, thresh1);

	//cout << "check surf2" << endl;

	if(good_matches.size()>=thresh2)  // draw matches and save image
	{
		Mat img_matches;
		drawMatches( img1, keypoints_1, img2, keypoints_2,
               good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
               vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );
		
		// Find homography
		vector<Point2f> pt1;
		vector<Point2f> pt2;

		for(int i = 0; i < good_matches.size(); i++)
		{
			pt1.push_back(keypoints_1[good_matches[i].queryIdx].pt);
			pt2.push_back(keypoints_2[good_matches[i].trainIdx].pt);
		}
		
		imwrite(output_str, img_matches);
		//imshow("output",img_matches);
		//waitKey(0);
		return 1;
	}
	else
	{
		printf("Time taken: %.2fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);
		cout << "no of matches12: " << match_12.size() << "\n";
		cout << "no of matches21: " << match_21.size() << "\n";
		cout << "no of good matches: " << good_matches.size() << "\n";
		cout << "Not enough good matches found" << "\n" << endl;
		int op;
		cin >> op;
	}

	//printf("logo does not exist in the input image\n");

  return 0;
 }

 
 int orbMatch(Mat img1, Mat img2, string output_str, double thresh1, int thresh2)
 {
	 Mat img_1, img_2; 
	cvtColor(img1, img_1, CV_RGB2GRAY);
	cvtColor(img2, img_2, CV_RGB2GRAY);

	if( !img_1.data || !img_2.data || !img1.data || !img2.data )
	{ std::cout<< " --(!) Error reading images " << std::endl; return -1; }

	clock_t tStart = clock();

	// ORB keypoints and descriptors
	ORB orb; //(50000, 1.1f, 8, 31, 0, 2, ORB::HARRIS_SCORE, 31); //(1000, 1.4f);

	//BRISK brisk;

	//FastFeatureDetector detector;

	vector<KeyPoint> keypoints_1, keypoints_2;

	//detector.detect(img_1, keypoints_1);
	//detector.detect(img_2, keypoints_2);

	Mat descriptors_1, descriptors_2;
	
	//BriefDescriptorExtractor extractor;
	//extractor.compute(img_1, keypoints_1, descriptors_1);
	//extractor.compute(img_2, keypoints_2, descriptors_2);

	orb.operator()(img_1, Mat(), keypoints_1, descriptors_1);
	orb.operator()(img_2, Mat(), keypoints_2, descriptors_2);

	cout << "no of ORB keypoints found in ref image: " << keypoints_1.size() << "\n";
	cout << "no of ORB keypoints found in test image: " << keypoints_2.size() << "\n";

	/*
	//BFMatcher
	BFMatcher bfmatch(NORM_HAMMING, true);
	
	vector< DMatch > good_matches;
	vector<vector<DMatch> > match_12, match_21;

	bfmatch.knnMatch(descriptors_1, descriptors_2, match_12, 2);
	bfmatch.knnMatch(descriptors_2, descriptors_1, match_21, 2);
	*/

	//Flann Based Matcher

	BFMatcher matcher;
	vector<DMatch> matches;
	matcher.match(descriptors_1, descriptors_2, matches);

	double max_dist = 0; double min_dist = 100;

	//-- Quick calculation of max and min distances between keypoints
	for( int i = 0; i < descriptors_1.rows; i++ )
	{ double dist = matches[i].distance;
	if( dist < min_dist ) min_dist = dist;
	if( dist > max_dist ) max_dist = dist;
	}

	printf("-- Max dist : %f \n", max_dist );
	printf("-- Min dist : %f \n", min_dist );

	//-- Draw only "good" matches (i.e. whose distance is less than 2*min_dist,
	//-- or a small arbitary value ( 0.02 ) in the event that min_dist is very
	//-- small)
	//-- PS.- radiusMatch can also be used here.
	vector< DMatch > good_matches;

	for( int i = 0; i < descriptors_1.rows; i++ )
	{ if( matches[i].distance <= max(4*min_dist, 0.02) )
	{ good_matches.push_back( matches[i]); }
	}	

	// number of matches found = good_matches.size() ... if more matches than certain threshold are found then the images should be matching
	if((int)good_matches.size()>=thresh2)  // draw matches and save image
	{
		Mat img_matches;
		drawMatches( img1, keypoints_1, img2, keypoints_2,
               good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
               vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );
		
		// Find homography
		vector<Point2f> pt1;
		vector<Point2f> pt2;

		for(int i = 0; i < (int)good_matches.size(); i++)
		{
			pt1.push_back(keypoints_1[good_matches[i].queryIdx].pt);
			pt2.push_back(keypoints_2[good_matches[i].trainIdx].pt);
		}
		
		printf("Time taken: %.2fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);
		cout << "no of good matches found: " << good_matches.size() << "\n";
		
		Mat img2write;
		resize(img_matches, img2write, Size(0,0), 0.1, 0.1);
		imwrite(output_str, img2write);
		
		return 1;
	}
	else
	{
		printf("Time taken: %.2fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);
		//cout << "no of matches12: " << match_12.size() << "\n";
		//cout << "no of matches21: " << match_21.size() << "\n";
		cout << "matches size: " << matches.size() << endl;		
		cout << "no of good matches: " << good_matches.size() << "\n";
		cout << "Not enough good matches found" << "\n" << endl;
		int op;
		cin >> op;
	}

	return 0;

 }

 int kazeMatch(Mat img1, Mat img2, string output_str, double thresh1, double thresh2)
 {
	 cout << "Applying KAZE image matching" << endl;

	 

	 Mat img_1, img_2; 	
	 cvtColor(img1, img_1, CV_RGB2GRAY);
	 cvtColor(img2, img_2, CV_RGB2GRAY);



	 if( !img_1.data || !img_2.data || !img1.data || !img2.data )
	 { cout<< " --(!) Error reading images " << std::endl; return -1; }

	 clock_t tStart = clock();

	 Mat image1_32, image2_32, descriptors_1, descriptors_2;
	 vector<KeyPoint> keypoints_1, keypoints_2;


	 cout << "check 1 " << endl;

	 KAZEOptions options;

	 img_1.convertTo(image1_32, CV_32F, 1.0/255.0, 0);
	 img_2.convertTo(image2_32, CV_32F, 1.0/255.0, 0);

	 cout << "check 2" << endl;

	 // Create the KAZE object
	 KAZE evolution(options);

	 cout << "check20" << endl;

	 // Create the nonlinear scale space
	 evolution.Allocate_Memory_Evolution();
	 cout << "memory allocated" << endl;
	 evolution.Create_Nonlinear_Scale_Space(image1_32);
	 cout << "non linear scale space created" << endl;
	 evolution.Feature_Detection(keypoints_1);
	 evolution.Compute_Descriptors(keypoints_1, descriptors_1);

	 cout << "check21 .. " << endl;

	 evolution.Create_Nonlinear_Scale_Space(image2_32);
	 cout << "non linear scale space created" << endl;
	 evolution.Feature_Detection(keypoints_2);
	 evolution.Compute_Descriptors(keypoints_2,descriptors_2);

	 cout << "check3" << endl;

	 //BFMatcher
	 BFMatcher bfmatch(NORM_HAMMING);
	
	 vector< DMatch > good_matches;
	 vector<vector<DMatch> > match_12, match_21;

	 bfmatch.knnMatch(descriptors_1, descriptors_2, match_12, 2);
	 bfmatch.knnMatch(descriptors_2, descriptors_1, match_21, 2);

	 if(match_12.size()>0 && match_21.size()>0)		
		 CheckNNDR(match_12, match_21, good_matches, thresh1);


	// number of matches found = good_matches.size() ... if more matches than certain threshold are found then the images should be matching
	if(good_matches.size()>=thresh2)  // draw matches and save image
	{
		Mat img_matches;
		drawMatches( img1, keypoints_1, img2, keypoints_2,
               good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
               vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );
		
		// Find homography
		vector<Point2f> pt1;
		vector<Point2f> pt2;

		for(int i = 0; i < good_matches.size(); i++)
		{
			pt1.push_back(keypoints_1[good_matches[i].queryIdx].pt);
			pt2.push_back(keypoints_2[good_matches[i].trainIdx].pt);
		}
		
		printf("Time taken: %.2fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);
		cout << "no of good matches found: " << good_matches.size() << "\n";
		imwrite(output_str, img_matches);
		//imshow("output",img_matches);
		//waitKey(0);
		return 1;
	}
	else
	{
		printf("Time taken: %.2fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);
		cout << "no of matches12: " << match_12.size() << "\n";
		cout << "no of matches21: " << match_21.size() << "\n";
		cout << "no of good matches: " << good_matches.size() << "\n";
		cout << "Not enough good matches found" << "\n" << endl;
		int op;
		cin >> op;
	}

	return 0;

 }




#endif // _FINDLOGO_H_